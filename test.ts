import test from 'ava'
import 'source-map-support/register'
import {
  createStateForLanguage,
  deflate,
  getConstants,
  getLanguages,
  processResponse,
  runRequest,
} from './'

const REQUEST_BODY = Buffer.from(
  'C8tJzEtnMGTISixLLE4uyiwo0c3LT0llcNNLBlJ6JZn5DEaGDMn5ecX5Oal6OfnpGkoZqTk5+Uqa1m56mXkFpSVgNQYMYYlF6cVAOggA',
  'base64'
)
const INPUT =
  'Vlang\u00001\u0000javascript-node\u0000F.code.tio\u000021\u0000console.log("hello");F.input.tio\u00000\u0000Vargs\u00000\u0000R'
const SAMPLE_RESPONSE = Buffer.from(
  'H4sIAAAAAAACAzNyD810Ccvxz0kzd/EI9MrJSM3JyecyQhPlCkpNzFEoycxNtVIw0DOwtFAo5gotTi1CCJmbAIWCK4v1EEKGpkAh54BQheKMxCKgkCVITEGVy7Uis0QhOT8FpArdIgCVOLTQjwAAAA==',
  'base64'
)

test('deflate', t => {
  const actual = deflate(INPUT)
  t.deepEqual(actual, REQUEST_BODY)
})

test('process response', t => {
  const ret = processResponse(SAMPLE_RESPONSE)
  t.deepEqual(ret.warnings, [])
  t.truthy(ret.output === 'hello\n')
})

test.skip('make request', async t => {
  const constants = await getConstants()
  const languages = await getLanguages(constants)
  const state = createStateForLanguage(languages, 'javascript-node')
  state.code = `console.log("hello")`

  const result = await runRequest(state, constants)
  t.is(result.output, 'hello\n')
})
