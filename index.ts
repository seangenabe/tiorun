import { ok } from 'assert';
import { randomBytes } from 'crypto';
import request from 'got';
import nonNull from 'non-null';
import { deflateRawSync, gunzipSync } from 'zlib';

// Reference:
// https://github.com/TryItOnline/tryitonline/blob/master/usr/share/tio.run/frontend.js */

const HOST = `https://tio.run`
const LANGUAGE_TOGGLES = {
  cflags: 'TIO_CFLAGS',
  options: 'TIO_OPTIONS',
  driver: 'TIO_DRIVER'
}

export async function getConstants(): Promise<IConstants> {
  // Scrape tio.run homepage
  const homepageRes = await request(`${HOST}/`, { encoding: 'utf8' })
  const homepageHtml = homepageRes.body

  const frontendJsUrl = nonNull(
    homepageHtml.match(
      /<script src="(\/static\/[0-9a-f]+\-frontend.js)" defer><\/script>/
    )
  )[1]

  const frontendJsRes = await request(`${HOST}${frontendJsUrl}`, {
    encoding: 'utf8'
  })
  const frontendJs = frontendJsRes.body

  const run = nonNull(
    frontendJs.match(/^var runURL = "\/cgi-bin\/static\/([^"]+)";$/m)
  )[1]
  const languagesJson = nonNull(
    frontendJs.match(
      /^languageFileRequest\.open\("GET", "\/static\/([^"]+)"\);$/m
    )
  )[1]

  return { run, languagesJson, host: HOST }
}

export async function getLanguages(
  constants: IConstants
): Promise<ILanguageList> {
  const response = await request(
    `${constants.host}/static/${constants.languagesJson}`,
    { json: true }
  )
  return response.body
}

export async function makeRequest(
  state: ITioState,
  constants: IConstants
): Promise<Buffer> {
  const runUrl = `${constants.host}/cgi-bin/static/${constants.run}`
  const token = getRandomBits(128)
  ok(state.data.lang.values[0], 'Language is not set.')

  const requestUrl = `${runUrl}${getSettings()}${token}`
  const res = await request.post(requestUrl, {
    body: deflate(stateToByteString(state)),
    encoding: null
  })
  return res.body
}

export function processResponse(responseBody: Buffer) {
  const response: string = gunzipSync(responseBody).toString()

  if (response.length < 32) {
    throw new Error(
      'Could not establish or maintain a connection with the server.'
    )
  }

  const results = response.substr(16).split(response.substr(0, 16))
  const warnings = nonNull(results.pop())
    .split('\n')
    .filter(Boolean)

  return { warnings, output: results[0], debug: results[1] }
}

export async function runRequest(state: ITioState, constants: IConstants) {
  const responseBody = await makeRequest(state, constants)
  return processResponse(responseBody)
}

export function getRandomBits(minBits) {
  return randomBytes((minBits + 7) >> 3).toString('hex')
}

function getSettings(...args: string[]) {
  const retArr: string[] = ['/']
  for (let arg of args) {
    if (typeof arg === 'string') {
      retArr.push(arg)
      retArr.push('/')
    }
  }
  return retArr.join('')
}

export function stateToByteString(state: ITioState) {
  const { header, code, footer } = state
  const realCode =
    (header ? `${header}\n` : '') + (code || '') + (footer ? `${footer}\n` : '')
  state.data['.code.tio'].value = realCode
  const retArr: string[] = []

  for (let [name, field] of Object.entries<ITioFieldV | ITioFieldF>(
    state.data as {}
  )) {
    if (field.disabled) {
      continue
    }
    retArr.push(field.type)
    retArr.push(`${name}\0`)
    if (field.type === 'F') {
      const value = textToByteString(field.value)
      retArr.push(`${value.length}\0${value}`)
    }
    if (field.type === 'V') {
      retArr.push(`${field.values.length}\0`)
      for (let value of field.values) {
        retArr.push(`${textToByteString(value)}\0`)
      }
    }
  }
  retArr.push('R')

  return retArr.join('')
}

function textToByteString(string: string) {
  return unescape(encodeURIComponent(string))
}

export function deflate(byteString: string): Buffer {
  return deflateRawSync(Buffer.from(byteString, 'binary'), { level: 9 })
}

export function createState(): ITioState {
  function v(...values: string[]): ITioFieldV {
    return { type: 'V', disabled: false, values }
  }
  function f(): ITioFieldF {
    return { type: 'F', disabled: false, value: '' }
  }
  return {
    data: {
      lang: v('') as ITioFieldV_Lang,
      TIO_CFLAGS: v(),
      TIO_OPTIONS: v(),
      TIO_DRIVER: v(),
      '.code.tio': f(),
      '.input.tio': f(),
      args: v()
    },
    header: '',
    code: '',
    footer: ''
  }
}

export function createStateForLanguage(
  languages: ILanguageList,
  languageId: string
) {
  return createStateForLanguageByIdData(languageId, languages[languageId])
}

export function createStateForLanguageByIdData(
  languageId: string,
  language: ILanguageData
): ITioState {
  const state = createState()
  const unmask = new Set(language.unmask || [])
  for (let [opt, dataKey] of Object.entries(LANGUAGE_TOGGLES)) {
    state.data[dataKey].disabled = !unmask.has(
      opt as keyof typeof LANGUAGE_TOGGLES
    )
  }
  state.data.lang.values[0] = languageId
  return state
}

interface ITioStateData {
  /** Language */
  lang: ITioFieldV_Lang
  /** Compiler flags */
  TIO_CFLAGS: ITioFieldV
  /** Command-line options */
  TIO_OPTIONS: ITioFieldV
  /** Driver */
  TIO_DRIVER: ITioFieldV
  /** Code */
  '.code.tio': ITioFieldF
  /** Input */
  '.input.tio': ITioFieldF
  /** Arguments */
  args: ITioFieldV
}

export interface ITioState {
  data: ITioStateData
  header: string
  code: string
  footer: string
}

export interface ITioFieldV_Lang extends ITioFieldV {
  values: [
    string // language code
  ]
}
export interface ITioFieldF {
  type: 'F'
  disabled: boolean
  value: string
}

export interface ITioFieldV {
  type: 'V'
  disabled: boolean
  values: string[]
}

export interface IConstants {
  run: string
  languagesJson: string
  host: string
}

export interface ILanguageList extends Record<string, ILanguageData> {}

export interface ILanguageData {
  categories: ('recreational' | 'practical')[]
  encoding: 'SBCS' | 'UTF-8'
  link: URLString
  name: string
  update: 'git' | 'manual'
  tests: {
    [testName: string]: {
      request: [
        {
          command: 'F' | 'V'
          payload: {}
        }
      ]
      response: string
    }
  }
  unmask?: (keyof typeof LANGUAGE_TOGGLES)[]
  prettify?: string
}

export type URLString = string
